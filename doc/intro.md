# Introduction to draw.clj

## Prerequisite

You will need the following to get the best experience:

  1. Java JDK 7 or above, recommeded 8.
  2. [Leiningen](https://leiningen.org/)

## Run

To run with leiningen (recommended if you have it):

    lein run

Or run with the jar:

    java -jar target/uberjar/draw.clj.jar

## Test

To run tests:

    lein test

And:

    lein run < test_input.txt > test_result.txt && diff expected_output.txt test_result.txt && echo "Match"

## Commands

| Command       | Description                                                                                                                                                                |
| :--           | :--                                                                                                                                                                        |
| C w h         | Create a new canvas of width w and height h.                                                                                                                               |
| L x1 y1 x2 y2 | Create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x' character. |
| R x1 y1 x2 y2 | Create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.         |
| B x y c       | Fill the entire area connected to (x,y) with "colour" c. The behaviour of this is the same as that of the "bucket fill" tool in paint programs.                            |
| Q             | Quit the program.                                                                                                                                                          |

## Compile

Compile the uberjar:

    lein uberjar

## Author

__Ray Cheung__
Twitter: [@rayswc](https://twitter.com/rayswc)
LinkedIn: [rayswc](linkedin.com/in/rayswc/)
