(defproject draw.clj "SNAPSHOT"
  :description "Simple drawing console program"
  :url "https://bitbucket.org/raysiuuuu/draw.clj"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [the-flood "0.1.1"]]
  :main ^:skip-aot draw.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :uberjar-name "draw.clj.jar"}})
