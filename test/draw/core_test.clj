(ns draw.core-test
  (:require [clojure.test :refer :all]
            [draw.core :refer :all]))

(deftest test-make-grid
  (let [g (make-grid 20 4)]
    (is (= 4 (alength g)))
    (is (= 20 (alength (first g))))))

(deftest test-draw-horizontal-line
  (let [g (make-grid 20 4)]
    (paint-line g 1 2 6 2)
    (doseq [x (range (dec 1) 6)]
      (is (= "x" (aget g (dec 2) x))))))

(deftest test-draw-vertical-line
  (let [g (make-grid 20 4)]
    (paint-line g 6 3 6 4)
    (doseq [y (range (dec 3) 4)]
      (is (= "x" (aget g y (dec 6)))))))

(deftest test-draw-rectangle
  (let [g (make-grid 20 4)]
    (paint-rect g 14 1 18 3)
    (doall
     (map #(doseq [x (range (dec 14) 18)]
             (is (= "x" (aget g % x))))
          [0 2]))
    (is (= "x" (aget g (dec 3) (dec 14))))
    (is (= "x" (aget g (dec 3) (dec 18))))
    (doseq [x (range (dec 15) 17)]
      (is (= " " (aget g (dec 2) x))))))

(deftest test-fill-area
  (let [g (make-grid 20 4)
        expected ["oooooooooooooxxxxxoo"
                  "xxxxxxooooooox   xoo"
                  "     xoooooooxxxxxoo"
                  "     xoooooooooooooo"]]
    (paint-line g 1 2 6 2)
    (paint-line g 6 3 6 4)
    (paint-rect g 14 1 18 3)
    (let [filled (->> (fill-area g 10 3 "o") (map vec) (mapv clojure.string/join))]
      (is (= expected filled)))))
