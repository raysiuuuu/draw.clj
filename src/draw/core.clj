(ns draw.core
  (:require [clojure.string :as string]
            [the-flood.core :refer [flood-fill]])
  (:gen-class))

(defn parse-int [s]
  "parse integer in string"
  (Integer/parseInt (re-find #"\A-?\d+" s)))

(defn make-grid [w h]
  "initialize a 2d grid"
  (assert (pos? w) "negative width")
  (assert (pos? h) "negative height")
  (to-array-2d (repeat h (repeat w " "))))

(defn draw-grid [grid]
  "print the grid"
  (if grid
    (let [b (string/join (repeat (->> (first grid) (alength) (+ 2)) "-"))]
      (println b)
      (doall (map #(println (str "|" (string/join (mapv str %)) "|")) grid))
      (println b))))

(defn paint-line [grid x1 y1 x2 y2 & {:keys [with only-if] :or {with "x" only-if nil}}]
  "draw a line :with char, :only-if the cell is not that yet"
  (assert (or (= x1 x2) (= y1 y2)) "not a line!")
  (assert (not= with only-if) "must not equal")
  (if (= y1 y2)
    (doseq [x (range (dec x1) x2)]
      (if (or (nil? only-if) (= only-if (aget grid (dec y1) x)))
        (aset grid (dec y1) x with)))
    (doseq [y (range (dec y1) y2)]
      (if (or (nil? only-if) (= only-if (aget grid y (dec x1))))
        (aset grid y (dec x1) with))))
  grid)

(defn paint-area [grid rx ry & {:keys [with] :or {with "x"}}]
  "paint a rectangular area on the grid with a character"
  (doseq [y ry] (doseq [x rx] (aset grid y x with))))

(defn paint-rect [grid x1 y1 x2 y2]
  "draw a rectangle"
  (paint-area grid (range (dec x1) x2) (range (dec y1) y2))
  (paint-area grid (range x1 (dec x2)) (range y1 (dec y2)) :with " ")
  grid)

; FIXME: this is impractical
; (defn flood-fill [grid x y t c]
;   "flood-fill an area, https://en.wikipedia.org/wiki/Flood_fill"
;   (when (and (not= t c) (pos? x) (< x (alength (first grid))) (pos? y) (< y (alength grid)))
;     (when (= t (aget grid y x))
;       (aset grid y x c))
;     (flood-fill grid x (inc y) t c)
;     (flood-fill grid x (dec y) t c)
;     (flood-fill grid (inc x) y t c)
;     (flood-fill grid (dec x) y t c)))

(defn fill-area [grid x y c]
  "fill an area like paintbrush"
  ; (flood-fill grid (dec x) (dec y) (aget grid (dec y) (dec x)) c)
  (to-array-2d (flood-fill (mapv vec grid) [(dec x) (dec y)] c)))

(defn -main
  "main loop"
  [& _args]
  (loop [grid (atom nil)]
    (print "enter command: ") (flush)
    (when-let [in (read-line)]
      (let [[cmd & cmd-args] (string/split in #" ")
            require-gird (fn [] (assert (not-empty @grid) "grid not present!"))]
        (when-not (= "Q" cmd)
          (try
            (case cmd
              "C" (->> cmd-args (take 2) (map parse-int) (apply make-grid) (reset! grid))
              "L" (do
                    (require-gird)
                    (->> cmd-args (take 4) (map parse-int) (apply swap! grid paint-line)))
              "R" (do
                    (require-gird)
                    (->> cmd-args (take 4) (map parse-int) (apply swap! grid paint-rect)))
              "B" (do
                    (require-gird)
                    (->> cmd-args (take 3) (map-indexed #(if (< % 2) (parse-int %2) %2))
                         (apply swap! grid fill-area)))
              (println "unknown operation, ignored."))
            (catch Exception ex (println "oops!" "ex:" ex)))
          (draw-grid @grid)
          (println)
          (recur grid))))))
